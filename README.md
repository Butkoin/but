
什麼是ButKoin？
-------------

ButKoin 是一種去中心化的安全加密貨幣，易於挖掘，不受 51% 攻擊，並提供簡單的個人之間交易方式。它是一種多算法硬幣。 ButKoin 使用智能節點抵押和獎勵系統來防止惡性通貨膨脹。

區塊瀏覽器：[https://explorer.butcoin.tech](http://explorer.butcoin.tech)

區塊瀏覽器-2：[http://blocks.butcoin.tech](http://blocks.butcoin.tech)

執照
-------

ButKoin Core 是根據 MIT 許可條款發布的。更多信息請參見[COPYING](COPYING)
信息或查看 https://opensource.org/licenses/MIT。

開發流程
-------------------

`master` 分支是為了穩定。開發是在不同的分支中完成的。
[Tags](https://gitee.com/ButKoin/but/tags) 被創建來表示新的官方，
But Core 的穩定發行版。

貢獻工作流在 [CONTRIBUTING.md](CONTRIBUTING.md) 中描述。

測試
-------

測試和代碼審查是開發的瓶頸；我們得到更多的拉力
我們可以在短時間內審查和測試請求。請耐心等待並通過測試提供幫助
其他人的拉取請求，請記住這是一個安全關鍵項目，任何錯誤都可能使人們付出代價
很多錢。

